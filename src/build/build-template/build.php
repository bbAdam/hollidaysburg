<?php

$distDirectory = "../../../dist/";
$templateName = trim($_POST['templateName']);
$templateDescription = trim($_POST['templateDescription']);
$templateAssetPath = trim($_POST['templateAssetPath']);

// START BY CREATING THE /dist DIRECTORY
mkdir($distDirectory, 0777);

$xmlFileContents = '';

$xmlName = str_replace(' ','-', $templateName);
$stylerXML = fopen($distDirectory . $xmlName . ".xml", "w") or die("Unable to create /dist/" . $xmlName . ".xml!");

//BEGIN THE XML FILE
$xmlFileContents .= '<?xml version="1.0" encoding="utf-8"?>
<Template ID="32">
  <AssetVirtualPath>/cms/' . $templateAssetPath . '/Centricity/Template/</AssetVirtualPath>
  <TemplateName><![CDATA[' . $templateName . ']]></TemplateName>
  <Description><![CDATA[' . $templateDescription . ']]></Description>
  <Responsive>False</Responsive>';

//OPEN THE XML HEAD CONTENT
$xmlFileContents .= '<HeadContent><![CDATA[';

//HEAD CONTENT CODE
// GET head.html
$headHTML = file_get_contents("../../template/html/head.html");

// GET COMPILED head.js
$headJS = file_get_contents("./template-files/head.js");

// UNCOMMENT THE ELEMENT CONTROLS
$headJS = preg_replace('/([\r\n\s]*\/\*)(["|\']*<swctrl.*?\/>["|\']*)(\*\/[\r\n\s]*?null)/i', '$2', $headJS);

// MERGE head.html and head.js
$xmlFileContents .= $headHTML . "\n\n" . '<script type="text/javascript">' . $headJS . '</script>';

//CLOSE THE XML HEAD CONTENT
$xmlFileContents .= ']]></HeadContent>';

//OPEN THE XML CSS
$xmlFileContents .= '<CSS><![CDATA[';

//PLACE CSS FILE INTO VARIABLE FOR ELEMENT REPLACEMENT
$cssDesktop = file_get_contents("./template-files/1024.css");
$css768 = file_get_contents("./template-files/768.css");
$css640 = file_get_contents("./template-files/640.css");
$css480 = file_get_contents("./template-files/480.css");
$css320 = file_get_contents("./template-files/320.css");

// UNCOMMENT THE ELEMENT CONTROLS
$cssDesktop = preg_replace('/([^\s]*\/\*)(.*?<swctrl.*?\/>.*?;)(\*\/)/i', '$2', $cssDesktop);
$css768 = preg_replace('/([^\s]*\/\*)(.*?<swctrl.*?\/>.*?;)(\*\/)/i', '$2', $css768);
$css640 = preg_replace('/([^\s]*\/\*)(.*?<swctrl.*?\/>.*?;)(\*\/)/i', '$2', $css640);
$css480 = preg_replace('/([^\s]*\/\*)(.*?<swctrl.*?\/>.*?;)(\*\/)/i', '$2', $css480);
$css320 = preg_replace('/([^\s]*\/\*)(.*?<swctrl.*?\/>.*?;)(\*\/)/i', '$2', $css320);

//CSS CODE
$xmlFileContents .= " /* MedaiBegin Standard */" . $cssDesktop . "/* MediaEnd *//* MediaBegin 768+ */ @media (max-width: 1023px) {" . $css768 . "} /* MediaEnd *//* MediaBegin 640+ */ @media (max-width: 767px) {" . $css640 . "} /* MediaEnd *//* MediaBegin 480+ */ @media (max-width: 639px) {" . $css480 . "} /* MediaEnd *//* MediaBegin 320+ */ @media (max-width: 479px) {" . $css320 . "} /* MediaEnd */";

//CLOSE THE XML CSS
$xmlFileContents .= ']]></CSS>';

//OPEN THE XML ELEMENTS
$xmlFileContents .=  '<CustomSettings><ElementSets>';

// BUILD ELEMENTS
// GET THE ELEMENTS FILE AND CONVERT THE JSON TO AN ASSOCIATIVE ARRAY
$elementsFile = file_get_contents("../../template/elements/elements.json");
$elements = json_decode($elementsFile, true);
$elementSets = "";

// LOOP THROUGH EACH ELEMENT SET 
foreach($elements as $elementSetName => $elementSet) {
    $elementSets .= '<ElementSet>' .
                        '<Name>' . trim($elementSetName) . '</Name>' .
                        '<Elements>';

    // LOOP THROUGH EACH ELEMENT IN THE SET
    if(is_array($elementSet)) {
        foreach($elementSet as $element) {
            $elementSets .= '<Element>' .
                                '<Name>' . trim($element["name"]) . '</Name>' .
                                '<Type>' . trim($element["type"]) . '</Type>' .
                                '<DefaultValue>' . trim($element["default"]) . '</DefaultValue>' .
                                '<Label>' . trim($element["label"]) . '</Label>' .
                                '<Description>' . trim($element["description"]) . '</Description>' .
                            '</Element>';
        }
    }

    $elementSets .=     '</Elements>' .
					'</ElementSet>';
}

$xmlFileContents .= $elementSets;

//CLOSE THE XML ELEMENTS
$xmlFileContents .= '</ElementSets></CustomSettings>';

//OPEN XML LAYOUT
$xmlFileContents .= '<Layouts>';

//OPEN XML HOMEPAGE CONTENT
$xmlFileContents .= '<Layout LayoutType="H"><![CDATA[';

//HOMEPAGE CODE
$xmlFileContents .= file_get_contents('../../template/html/hp.html');

//CLOSE XML HOMEPAGE CONTENT
$xmlFileContents .= ']]></Layout>';

//OPEN XML SUBPAGE NO NAV CONTENT
$xmlFileContents .= '<Layout LayoutType="N"><![CDATA[';

//SUBPAGE CODE
$xmlFileContents .= file_get_contents('../../template/html/spn.html');

//CLOSE XML SUBPAGE NO NAV CONTENT
$xmlFileContents .= ']]></Layout>';

//OPEN XML MY VIEW CONTENT
$xmlFileContents .= '<Layout LayoutType="P"><![CDATA[';

//MY VIEW CODE
$xmlFileContents .= file_get_contents('../../template/html/mv.html');

//CLOSE XML MY VIEW CONTENT
$xmlFileContents .= ']]></Layout>';

//OPEN XML SUBPAGE CONTENT
$xmlFileContents .= '<Layout LayoutType="S"><![CDATA[';

//SUBPAGE CODE
$xmlFileContents .= file_get_contents('../../template/html/sp.html');

//CLOSE XML SUBPAGE CONTENT
$xmlFileContents .= ']]></Layout>';

//CLOSE XML LAYOUT
$xmlFileContents .= '</Layouts>';

//END THE XML FILE
$xmlFileContents .= '</Template>';

//REPLACE URL PATHS ACCROSS THE ENTIRE TEMPLATE
$xmlFileContents = preg_replace('/(\.\/|\/)*template\/assets\//i', '/cms/' . $templateAssetPath . '/Centricity/Template/32', $xmlFileContents);

fwrite($stylerXML, $xmlFileContents);
fclose($stylerXML);

// COPY TEMPLATE ASSETS
mkdir($distDirectory . "Assets", 0777);
$assetsDir = scandir("../../template/assets/");
foreach($assetsDir as $file) {
    if ($file != "." && $file != "..") {
        copy("../../template/assets/" . $file, $distDirectory . "Assets/" . $file);
    }
}

// Start the backup!
zipData($distDirectory, $distDirectory . $xmlName . '.zip');
//echo 'Finished.';
// Here the magic happens :)
function zipData($source, $destination) {
  if (extension_loaded('zip')) {
    if (file_exists($source)) {
      $zip = new ZipArchive();
      if ($zip->open($destination, ZIPARCHIVE::CREATE)) {
        $source = realpath($source);
        if (is_dir($source)) {
          $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
          foreach ($files as $file) {
            $file = realpath($file);
            if (is_dir($file)) {
              //$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            } else if (is_file($file)) {
              $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
          }
        } else if (is_file($source)) {
          $zip->addFromString(basename($source), file_get_contents($source));
        }
      }
      return $zip->close();
    }
  }
  return false;
}

// REMOVE ALL TEMPORARY FILES
$tempFiles = array(
    "./template-files/head.js",
    "./template-files/head-to-compile.js",
    "./template-files/1024-to-compile.scss",
    "./template-files/768-to-compile.scss",
    "./template-files/640-to-compile.scss",
    "./template-files/480-to-compile.scss",
    "./template-files/320-to-compile.scss",
    "./template-files/1024.css",
    "./template-files/768.css",
    "./template-files/640.css",
    "./template-files/480.css",
    "./template-files/320.css",
    "./template-files/css-compile.js",
    $distDirectory . $xmlName . ".xml"
);
foreach($tempFiles as $file) {
    unlink($file);
}

// DELETE THE ASSETS FOLDER
$distAssets = scandir($distDirectory . "Assets/");
foreach($distAssets as $file) {
    if ($file != "." && $file != "..") {
        unlink($distDirectory . "Assets/" . $file);
    }
}
rmdir($distDirectory . "Assets/");

echo json_encode(array("msg" => "Your template has been built successfully! It's available in the /dist directory. Rock and roll!", "error" => false));

?>