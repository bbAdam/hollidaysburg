<?php

// GET head.html
$headHTML = file_get_contents("../../template/html/head.html");

// GET COMPILED head.js
$headJS = file_get_contents("../../template/compiled/js/head.js");

// MERGE head.html and head.js
$mergedHead = $headHTML . "\n\n" . '<script type="text/javascript">' . $headJS . '</script>';
$mergedHeadFile = fopen("../../template/compiled/html/head.html", "w") or die("Unable to open file!");
fwrite($mergedHeadFile, $mergedHead);
fclose($mergedHeadFile);

// REMOVE ALL TEMPORARY FILES
$tempFiles = array(
    "../../template/compiled/js/head.js",
    "../../template/compiled/js/head-to-compile.js",
    "../../template/compiled/css/1024-to-compile.scss",
    "../../template/compiled/css/768-to-compile.scss",
    "../../template/compiled/css/640-to-compile.scss",
    "../../template/compiled/css/480-to-compile.scss",
    "../../template/compiled/css/320-to-compile.scss",
    "../../template/compiled/css/css-compile.js"
);
foreach($tempFiles as $file) {
    unlink($file);
}

echo json_encode(array("msg" => "JavaScript and SASS compiling complete. Ready for page refresh.", "error" => false));

?>