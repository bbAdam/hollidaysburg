// https://www.npmjs.com/package/sync-request
const request = require('sync-request');

// GET THE localHostPath VALUE FROM package.json
// THE REGEX REMOVES ANY "/" CHARACTERS FROM THE BEGINNING AND END OF THE STRING SO THAT WE CAN WORK WITH A PREDICTABLE VALUE
const pJson = require('../../../package.json');
let localHostPath = pJson.localHostPath.replace(/(^\/?)(.*?)(\/?$)/gi, '$2');
if(localHostPath == "") {
    localHostPath = "/";
} else {
    localHostPath = "/" + localHostPath + "/";
}

// NODE SYNCHRONOUS AJAX REQUEST
let res = request('GET', 'http://localhost:8888' + localHostPath + 'src/build/compile-template/compile.php');
var response = JSON.parse(res.getBody('utf8'));

// ADD COLORED TEXT TO TERMINAL - FIRST \x1b[31m TO SET COLOR TO RED AND THEN \x1b[0m TO RESET BACK TO DEFAULT TERMINAL COLOR.
// https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
if(response.error) {
    console.log("\x1b[31m", response.msg, "\x1b[0m");
} else {
    console.log("\x1b[32m", response.msg, "\x1b[0m");
}