<?php
// PREP THE JAVASCRIPT FIRST
// GET head.html
$headJS = file_get_contents("../../template/js/head.js");

// RENDER THE ACTIVE BLOCKS
// GET THE ELEMENTS FILE AND CONVERT THE JSON TO AN ASSOCIATIVE ARRAY
$elementsArray = array();
$elementsFile = file_get_contents("../../template/elements/elements.json");
$elements = json_decode($elementsFile, true);

// LOOP THROUGH EACH ELEMENT SET 
foreach($elements as $elementSet) {
    // LOOP THROUGH EACH ELEMENT IN THE SET
    if(is_array($elementSet)) {
        foreach($elementSet as $element) {
            $elementValue = ($element["type"] == "dropdown") ? (explode(";", $element["default"]))[0] : $element["default"];
            $elementsArray['<SWCtrl controlname="Custom" props="Name:' . $element["name"] . '" />'] = $elementValue;
        }
    }
}

// REPLACE ALL OF THE ELEMENTS WITH THEIR VALUES
$headJS = strtr($headJS, $elementsArray);

// REMOVE ANY CONTROLS THAT ARE LEFT - NOT DEFINED IN THE elements.json FILE
$headJS = preg_replace('/<swctrl.*?\/>/i', "", $headJS);

// WRITE THE UPDATED JAVASCRIPT BACK TO A TEMPORARY FILE FOR BABEL
$updatedHeadJS = fopen("../../template/compiled/js/head-to-compile.js", "w") or die("Unable to create /src/template/compiled/js/head-to-compile.js");
fwrite($updatedHeadJS, $headJS);
fclose($updatedHeadJS);

// NOW PREP THE CSS FILES
$cssFiles = array(
    "1024" => "../../template/css/1024.scss",
    "768" => "../../template/css/768.scss",
    "640" => "../../template/css/640.scss",
    "480" => "../../template/css/480.scss",
    "320" => "../../template/css/320.scss"
);

$cssFile = "";
foreach($cssFiles as $breakpoint => $file) {
    $cssFile = file_get_contents($file);
    $cssFile = strtr($cssFile, $elementsArray);
    // REMOVE ANY CONTROLS THAT ARE LEFT - NOT DEFINED IN THE elements.json FILE
    $cssFile = preg_replace('/<swctrl.*?\/>/i', "", $cssFile);

    $updatedCSS = fopen("../../template/compiled/css/$breakpoint-to-compile.scss", "w") or die("Unable to create /src/template/compiled/css/$breakpoint-to-compile.css");
    fwrite($updatedCSS, $cssFile);
    fclose($updatedCSS);
}

echo json_encode(array("msg" => "Element controls rendered successfully.", "error" => false));

?>