$(function() {
    CreativeTemplate.Init();
});

var CreativeTemplate = {
    // PROPERTIES
    "KeyCodes": { "tab": 9, "enter": 13, "esc": 27, "space": 32, "end": 35, "home": 36, "left": 37, "up": 38, "right": 39, "down": 40 },
    "IsMyViewPage": false,
    
    // METHODS
    "Init": function() {
        // FOR SCOPE
        var _this = this;
        
        this.SetTemplateProps();
        this.JsMediaQueries();
        this.MyStart();
        this.Header();
        this.ChannelBar();
        this.Body();
        this.GlobalIcons();
        this.SocialIcons();
        this.ModEvents();
        this.Footer();
        this.Slideshow();
        this.RsMenu();
        this.AppAccordion();
        this.Search();
        this.GlobalJs("NewWindowLinks");
        
        $(window).load(function(){ _this.WindowLoad(); });
        $(window).resize(function(){ _this.WindowResize(); });
    },
    
    "SetTemplateProps": function() {
        // MYVIEW PAGE CHECK
        if($(".myview").length) this.IsMyViewPage = true;
    },

    "WindowLoad": function() {
        this.GlobalJs("NewWindowLinks");
    },

    "WindowResize": function() {
        this.JsMediaQueries();
    },
    
    "GlobalJs": function(action) {
        if(action.indexOf("TcwMarkdown") > -1) {
            csGlobalJs.TcwMarkDown();
        }
        
        if(action.indexOf("NewWindowLinks") > -1) {
            csGlobalJs.OpenInNewWindowWarning();
        }
    },

    "JsMediaQueries": function() {
        switch(this.GetBreakPoint()) {
            case "desktop": 

            break;
            case "768":

            break;
            case "640":

            break;
            case "480":

            break;
            case "320":

            break;
        }
    },

    "MyStart": function() {
        // ADD TRANSLATE
        $("#gb-mystart .gb-mystart.left").creativeTranslate({
            "type": 2
        });
    },
    
    "DropdownActions": function(params) {
        /* ASSUMES THE FOLLOWING STRUCTURE
        <div class="cs-mystart-dropdown user-options">
            <div class="cs-selector" tabindex="0" aria-label="User Options" role="button" aria-expanded="false" aria-haspopup="true">User Options</div>
            <div class="cs-dropdown" aria-hidden="true" style="display:none;">
                <ul class="cs-dropdown-list">
                    <li><a href="#">Link</a></li>
                </ul>
            </div>
        </div>
        */
        
        // FOR SCOPE
        var _this = this;

        var dropdownParent = params.dropdownParent;
        var dropdownSelector = params.dropdownSelector;
        var dropdown = params.dropdown;
        var dropdownList = params.dropdownList;
        
        $(dropdownParent + " " + dropdownList + " a").attr("tabindex", "-1");
        
        // MYSTART DROPDOWN SELECTOR CLICK EVENT
        $(dropdownParent).on("click", dropdownSelector, function(e) {
            e.preventDefault();
            
            if($(this).parent().hasClass("open")){
                $("+ " + dropdownList + " a").attr("tabindex", "-1");
                $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
            } else {
                $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden","false").slideDown(300, "swing");
            }
        });
        
        // MYSTART DROPDOWN SELECTOR KEYDOWN EVENTS
        $(dropdownParent).on("keydown", dropdownSelector, function(e) {
            // CAPTURE KEY CODE
            switch(e.keyCode) {
                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.enter:
                case _this.KeyCodes.space:
                    e.preventDefault();

                    // IF THE DROPDOWN IS OPEN, CLOSE IT
                    if($(dropdownParent).hasClass("open")){
                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing"); 
                    } else {
                        $(this).attr("aria-expanded", "true").parent().addClass("open").find(dropdown).attr("aria-hidden", "false").slideDown(300, "swing", function(){
                            $(dropdownList + " li:first-child a", this).attr("tabindex", "0").focus();
                        });
                    }
                break;

                // CONSUME TAB KEY
                case _this.KeyCodes.tab:
                    if($("+ " + dropdown + " " + dropdownList + " a").length) {
                        $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                        $(this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                    }
                break;

                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.down:
                case _this.KeyCodes.right:
                    e.preventDefault();
                    
                    $("+ " + dropdown + " " + dropdownList + " a").attr("tabindex", "-1");
                    $("+ " + dropdown + " " + dropdownList + " li:first-child > a", this).attr("tabindex", "0").focus();
                break;
            }
        });
        
        // MYSTART DROPDOWN LINK KEYDOWN EVENTS
        $(dropdownParent).on("keydown", dropdownList + " li a", function(e) {
            // CAPTURE KEY CODE
            switch(e.keyCode) {
                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.left:
                case _this.KeyCodes.up:
                    e.preventDefault();

                    // IS FIRST ITEM
                    if($(this).parent().is(":first-child")) {
                        // FOCUS DROPDOWN BUTTON
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                    } else {
                        // FOCUS PREVIOUS ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).parent().prev("li").find("> a").attr("tabindex", "0").focus();
                    }
                break;

                // CONSUME RIGHT AND DOWN ARROWS
                case _this.KeyCodes.right:
                case _this.KeyCodes.down:
                    e.preventDefault();

                    // IS LAST ITEM
                    if($(this).parent().is(":last-child")) {
                        // FOCUS FIRST ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                    } else {
                        // FOCUS NEXT ITEM
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).parent().next("li").find("> a").attr("tabindex", "0").focus();
                    }
                break;
                
                // CONSUME TAB KEY
                case _this.KeyCodes.tab:
                    if(e.shiftKey) {
                        e.preventDefault();
                        
                        // FOCUS DROPDOWN BUTTON
                        $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                        $(this).closest(dropdownParent).find(dropdownSelector).focus();
                    }
                break;

                // CONSUME HOME KEY
                case _this.KeyCodes.home:
                    e.preventDefault();

                    // FOCUS FIRST ITEM
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).closest(dropdownList).find("li:first-child > a").attr("tabindex", "0").focus();
                break;

                // CONSUME END KEY
                case _this.KeyCodes.end:
                    e.preventDefault();

                    // FOCUS LAST ITEM
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(this).closest(dropdownList).find("li:last-child > a").attr("tabindex", "0").focus();
                break;
                
                // CONSUME ESC KEY
                case _this.KeyCodes.esc:
                    e.preventDefault();

                    // FOCUS DROPDOWN BUTTON AND CLOSE DROPDOWN
                    $(this).closest(dropdownParent).find(dropdownSelector).focus();
                    $(this).closest(dropdownList).find("a").attr("tabindex", "-1");
                    $(dropdownSelector).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                break;
            }
        });
        
        $(dropdownParent).mouseleave(function(e) {
            if(e.target.nodeName.toLowerCase() !== "select") {
                $(dropdownList + " a", this).attr("tabindex", "-1");
                $(dropdownSelector, this).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
            }
        }).focusout(function() {
            var thisDropdown = this;
            
            setTimeout(function () {
                if(!$(thisDropdown).find(":focus").length) {
                    $(dropdownSelector, thisDropdown).attr("aria-expanded", "false").parent().removeClass("open").find(dropdown).attr("aria-hidden", "true").slideUp(300, "swing");
                }
            }, 500);
        });
    },

    "Header": function() {
        // ADD LOGO
        var showLogo = <SWCtrl controlname="Custom" props="Name:showLogo" />;
        var logoSrc = $.trim("<SWCtrl controlname="Custom" props="Name:logoSrc" />");
        if(showLogo) {
            if((logoSrc != "") && logoSrc.indexOf("default-man") == -1) {
                $("#gb-logo").append('<a href="/[$SITEALIAS$]"><img src="' + logoSrc + '" alt="[$SiteName$] logo" /></a>');
            } else {
                $("#gb-logo").append('<a href="/[$SITEALIAS$]"><img src="/cms/lib9/SWCS000001/Centricity/template/83/defaults/phoenix.svg" width="63" height="102" alt="[$SiteName$] logo" /></a>');
            }
        }

        // ADD SITENAME
        var siteNameOne = $.trim("<SWCtrl controlname="Custom" props="Name:siteNameOne" />");
        var siteNameTwo = $.trim("<SWCtrl controlname="Custom" props="Name:siteNameTwo" />");
        var siteNameBegin = '';
        var siteNameEnd = '';

        if((siteNameOne == "") && (siteNameTwo == "")) {
            var splitLen = 2;
            var siteName = "[$SiteName$]";
            siteName = siteName.split(" ");
            var siteNameLength = siteName.length;
            if(siteNameLength > 2){
                siteNameEnd = $.trim(siteName.splice(-splitLen, siteName.length).toString().replace(/,/g, " "));
            } else {
                siteNameEnd = $.trim(siteName.splice(-1, siteName.length).toString().replace(/,/g, " "));
            }
            siteNameBegin = $.trim(siteName.toString().replace(/,/g, " "));
            $("#gb-sitename").prepend("<h1>" + siteNameBegin + "<span>" + siteNameEnd + "</span></h1>");
        } else if((siteNameOne != "") && (siteNameTwo == "")) {
            $("#gb-sitename").prepend("<h1>" + siteNameOne + "</h1>");
        } else if((siteNameOne == "") && (siteNameTwo != "")) {
            $("#gb-sitename").prepend("<h1>" + siteNameTwo + "</h1>");
        } else if((siteNameOne != "") && (siteNameTwo != "")) {
            $("#gb-sitename").prepend("<h1>" + siteNameBegin + "<span>" + siteNameEnd + "</span></h1>");
        }
    },

    "ChannelBar": function() {
        $(".sw-channel-item").unbind("hover");
        $(".sw-channel-item").hover(function(){
            $(".sw-channel-item ul").stop(true, true);
            var subList = $(this).children('ul');
            if ($.trim(subList.html()) !== "") {
                subList.slideDown(300, "swing");
            }
            $(this).addClass("hover");
        }, function(){
            $(".sw-channel-dropdown").slideUp(300, "swing");
            $(this).removeClass("hover");
        });
    },

    "Body": function() {
        // AUTO FOCUS SIGN IN FIELD
        $("#swsignin-txt-username").focus();

        // ADD READ FULL STORY BUTTON TO EACH HEADLINE
        $(".ui-widget.app.headlines").each(function() {
            $(".ui-article", this).each(function() {
                var headlineURL = $("h1 a", this).attr("href");
                $(".ui-article-description", this).append('<a class="custom-read-more" href="' + headlineURL + '" aria-hidden="true" tabindex="-1">Read Full Story »</a>');
            });
        });

        // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
        $("div.ui-widget.app .ui-widget-detail img")
            .not($("div.ui-widget.app.multimedia-gallery .ui-widget-detail img"))
            .not($("div.ui-widget.app.gallery.json .ui-widget-detail img"))
            .each(function() {
                if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                    $(this).css({"display": "inline-block", "width": "100%", "max-width": $(this).attr("width") + "px", "height": "auto", "max-height": $(this).attr("height") + "px"});
                }
        });

        // ADJUST FIRST BREADCRUMB
        $("li.ui-breadcrumb-first > a > span").text("Home");

        // USE CHANNEL NAME FOR PAGELIST NAV HEADER IF ONE IS NOT PRESENT
        if(!$("div.sp.column.one .ui-widget-header h1").length) {
            $("div.sp.column.one .ui-widget-header").append("<h1>[$ChannelName$]</h1>");
        }
    },
    
    "GlobalIcons": function() {
        
    },
    
    "SocialIcons": function() {
        var socialIcons = [
            
        ];

        var icons = '';
        $.each(socialIcons, function(index, icon) {
            if(icon.show) {
                icons += '<li><a class="gb-social-icon ' + icon.class + '" href="' + icon.url + '" target="' + icon.target + '" aria-label="' + icon.label + '"></a></li>';
            }
        });

        if(icons.length) {
            $("#gb-social-icons").html('<ul>' + icons + '</ul>');
        }
    },
    
    "ModEvents": function() {
        $(".ui-widget.app.upcomingevents").modEvents({
            columns     : "yes",
            monthLong   : "yes",
            dayWeek     : "yes"
        });
        
        $(".ui-widget.app.upcomingevents .ui-article").each(function() {
            if(!$(".ui-article-title.sw-calendar-block-date", this).length && $.trim($(".ui-article-description", this).text()) != "There are no upcoming events to display.") {
                var moveArticle = $(this).html();
                $(this).parent().prev("li").find(".ui-article").append(moveArticle);
                $(this).parent().remove();
            } else {
                var eventMonth = $(".joel-month", this).detach();
                var eventDay = $(".joel-day", this).detach();
                var eventDate = $.trim($(".ui-article-title", this).text());
                if(eventDate.length == 1) eventDate = "0" + eventDate;
                
                var ariaDate = eventDate;
                switch(eventDate) {
                    case "01": ariaDate = "First"; break;
                    case "02": ariaDate = "Second"; break;
                    case "03": ariaDate = "Third"; break;
                    case "04": ariaDate = "Fourth"; break;
                    case "05": ariaDate = "Fifth"; break;
                    case "06": ariaDate = "Sixth"; break;
                    case "07": ariaDate = "Seventh"; break;
                    case "08": ariaDate = "Eighth"; break;
                    case "09": ariaDate = "Ninth"; break;
                    case "10": ariaDate = "Tenth"; break;
                    case "11": ariaDate = "Eleventh"; break;
                    case "12": ariaDate = "Twelveth"; break;
                    case "13": ariaDate = "Thirteenth"; break;
                    case "14": ariaDate = "Fourteenth"; break;
                    case "15": ariaDate = "Fifteenth"; break;
                    case "16": ariaDate = "Sixteenth"; break;
                    case "17": ariaDate = "Seventeenth"; break;
                    case "18": ariaDate = "Eighteenth"; break;
                    case "19": ariaDate = "Nineteenth"; break;
                    case "20": ariaDate = "Twentieth"; break;
                    case "21": ariaDate = "Twenty-first"; break;
                    case "22": ariaDate = "Twenty-second"; break;
                    case "23": ariaDate = "Twenty-third"; break;
                    case "24": ariaDate = "Twenty-fourth"; break;
                    case "25": ariaDate = "Twenty-fifth"; break;
                    case "26": ariaDate = "Twenty-sixth";  break;
                    case "27": ariaDate = "Twenty-seventh"; break;
                    case "28": ariaDate = "Twenty-eighth"; break;
                    case "29": ariaDate = "Twenty-ninth"; break;
                    case "30": ariaDate = "Thirtieth"; break;
                    case "31": ariaDate = "Thirty-first"; break;
                }
                
                $(".ui-article-title", this).html(eventMonth).prepend('<span class="joel-date" aria-hidden="true">' + eventDate + '</span>').append('<span class="joel-day" aria-hidden="true">' + $(eventDay).text() + '</span>').attr("aria-label", $(eventDay).text() + ", " + $(eventMonth).text() + " " + ariaDate);
                $(".joel-month", this).attr("aria-hidden", "true");
            }
        });
    },

    "Footer": function() {
        // MOVE Bb FOOTER STUFF
        $(".gb-schoolwires-footer.logo").html($("#sw-footer-logo").html());
        var schoolwiresLinks = '';
        schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(0)").html().replace("|", "")) + '</li>';
        schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(2)").html().replace("|", "")) + '</li>';
        schoolwiresLinks += '<li>' + $.trim($("#sw-footer-links li:eq(1)").html().replace("|", "")) + '</li>';
        $(".gb-schoolwires-footer.links").append('<ul>' + schoolwiresLinks + '</ul>');
        $(".gb-schoolwires-footer.copyright").append($("#sw-footer-copyright").html());
    },

    "Slideshow": function() {
        if($("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery").length) {
            var mmg = eval("multimediaGallery" + $("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery:first").attr("data-pmi"));
            mmg.props.defaultGallery = false;

            $("#sw-content-container10.ui-hp .ui-widget.app.multimedia-gallery:first").csMultimediaGallery({
                "efficientLoad" : true,
                "imageWidth" : 960,
                "imageHeight" : 500,
                "mobileDescriptionContainer": [640, 480, 320], // [960, 768, 640, 480, 320]
                "galleryOverlay" : false,
                "linkedElement" : [], // ["image", "title", "overlay"]
                "playPauseControl" : false,
                "backNextControls" : false,
                "bullets" : false,
                "thumbnails" : false,
                "thumbnailViewerNum": [4, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                "autoRotate" : true,
                "hoverPause" : true,
                "transitionType" : "fade", // fade, slide, custom
                "transitionSpeed" : 1.5,
                "transitionDelay" : 4,
                "fullScreenRotator" : false,
                "fullScreenBreakpoints" : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                "onImageLoad" : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                "allImagesLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onTransitionStart" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
                "onTransitionEnd" : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                "allLoaded" : function(props) {}, // props.element, props.mmgRecords
                "onWindowResize": function(props) {} // props.element, props.mmgRecords
            });
        }
    },

    "RsMenu": function() {
        $.csRsMenu({
            "breakPoint" : 480, // SYSTEM BREAK POINTS - 768, 640, 480, 320
            "slideDirection" : "left-to-right", // OPTIONS - left-to-right, right-to-left
            "menuButtonParent" : "#sw-mystart-left",
            "menuBtnText" : "Menu",
            "colors": {
                "pageOverlay": "#000000", // DEFAULT #000000
                "menuBackground": "#FFFFFF", // DEFAULT #FFFFFF
                "menuText": "#333333", // DEFAULT #333333
                "menuTextAccent": "#333333", // DEFAULT #333333
                "dividerLines": "#E6E6E6", // DEFAULT #E6E6E6
                "buttonBackground": "#E6E6E6", // DEFAULT #E6E6E6
                "buttonText": "#333333" // DEFAULT #333333
            },
            "showSchools" : false,
            "schoolMenuText": "Our Schools",
            "showTranslate" : false,
            "translateMenuText": "Translate Language",
            "translateVersion": 1, // 1 = FRAMESET, 2 = BRANDED
            "translateId" : "",
            "showAccount": true,
            "accountMenuText": "User Options",
            "usePageListNavigation": true,
            "extraMenuOptions": {
                /*
                "Menu Name": [
                    {
                        "text": "",
                        "url": "",
                        "target": ""
                    }
                ]
                */
            }, 
            "siteID": "[$siteID$]",
            "allLoaded": function(){}
        });
    },

    "AppAccordion": function() {
        $("#hp-content").csAppAccordion({
            "accordionBreakpoints" : [640, 480, 320]
        });

        $(".sp-column.one").csAppAccordion({
            "accordionBreakpoints" : [480, 320]
        });
    },

    "Search": function() {
        var _this = this;
        
        $(document).on("click keydown", "#gb-search-button", function(e){
            if(_this.AllyClick(e)) {
                e.preventDefault();
                
                $("#gb-search-form").submit();
            }
        });
        
        $("#gb-search-form").submit(function(e){
            e.preventDefault();
            
            if($.trim($("#gb-search-input").val()) != "" && $.trim($("#gb-search-input").val()) != "Search...") {
                window.location.href = "/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=" + $("#gb-search-input").val();
            }
        });
        
        $("#gb-search-input").focus(function() {
            if($(this).val() == "Search...") {
                $(this).val("");
            }
        }).blur(function() {
            if($(this).val() == "") {
                $(this).val("Search...");
            }
        });
    },

    "AllyClick": function(event) {
        if(event.type == "click") {
            return true;
        } else if(event.type == "keydown" && (event.keyCode == this.KeyCodes.space || event.keyCode == this.KeyCodes.enter)) {
            return true;
        } else {
            return false;
        }
    },

    "GetBreakPoint": function() {
        // 320
        if(window.matchMedia("(max-width: 479px)").matches) { return "320"; }

        // 480
        else if(window.matchMedia("(max-width: 639px)").matches) { return "480"; }

        // 640
        else if(window.matchMedia("(max-width: 767px)").matches) { return "640"; }

        // 768
        else if(window.matchMedia("(max-width: 1023px)").matches) { return "768"; }

        // DESKTOP
        else { return "desktop"; }
    }
};