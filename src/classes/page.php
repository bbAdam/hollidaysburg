<?php

class Page {

    function __construct() {
        $variables = new Variables();
        $replacements = new Replacements();
        $errorPage = new ErrorPage();

        // SET PROPERTIES
        $this->siteDomain = $variables->Get("siteDomain");
        $this->homepageUrl = $variables->Get("homepageUrl");
        $this->subpageUrl = $variables->Get("subpageUrl");
        $this->subpageNoNavUrl = $variables->Get("subpageNoNavUrl");
        $this->replacements = $replacements;
        $this->errorPage = $errorPage;
    }

    public function BuildPage() {
        // CHECK FOR CORRECT PORT
        $this->CheckPort();

        // BUILD THE PAGE URL
        $this->BuildPageUrl();

        // GET AND BUILD THE PAGE
        $page = $this->GetPage();

        // RETURN THE BUILT PAGE
        return $page;
    }

    private function CheckPort() {
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url = preg_match('/htt.*?localhost:([0-9]{4})/i', $url, $port, PREG_OFFSET_CAPTURE);
        $port = $port[1][0];

        if($port != "8888") {
            echo $this->errorPage->PrintError('There are some dependecies in this development environment that require the localhost port be set to 8888. Please open your MAMP preferences and under "Ports", set "8888" for "Apache Port"');

            exit;
        }
    }

    private function BuildPageUrl() {
        // FIRST CHECK FOR MALFORMED URL
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $urlValues = array_count_values(str_split($url));
        // MULTIPLE ? CHARACTERS IN THE URL WILL GENERATE A PAGE ERROR AND ALSO GENERATED A BAD CACHE PAGE AND WILL THEN THROW ERRORS WHEN TRYING TO CLEAR THE CACHE FILES
        if(array_key_exists("?", $urlValues) && $urlValues["?"] > 1) {
            echo $this->errorPage->PrintError('You have multiple "?" characters in your url query string. Change the duplicates to "&" and refresh your page.');

            exit;
        }

        // SET OUR VARIABLES
        $loggedIn = (isset($_GET["loggedin"]) && $_GET["loggedin"] == "true") ? true : false;
        $pageID = (isset($_GET["page"])) ? $_GET["page"] : "hp";
        $pageUrl = $this->siteDomain . $this->homepageUrl;

        switch($pageID) {
            case "sp":
                $pageUrl = $this->siteDomain . $this->subpageUrl;
            break;

            case "spn":
            case "mv":
                $pageUrl = $this->siteDomain . $this->subpageNoNavUrl;
            break;
        }

        $this->loggedIn = $loggedIn;
        $this->pageID = $pageID;
        $this->pageUrl = $pageUrl;
    }

    private function GetPage() {
        if(file_exists("./src/cache/$this->pageID.php")) {
            // GET PAGE FROM CACHE
            $page = file_get_contents("./src/cache/$this->pageID.php");
        } else {
            // REQUEST THE PAGE
            $page = $this->RequestPage();

            // ADJUST SYSTEM FILES AS NEEDED - SEE /includes/update-files.php FOR EACH INDIVIDUAL UPDATE 
            $page = $this->UpdateFiles($page);

            // EXECUTE ALL OF THE NECESSARY SYSTEM REPLACEMENTS - STORED IN /includes/replacements.php 
            $page = $this->RunSystemReplacements($page);
        }

        // INSERT THE TEMPLATE FILES
        // WE ALSO EXECUTE ALL OF THE NECESSARY TEMPLATE REPLACEMENTS - STORED IN /includes/replacements.php 
        $page = $this->InsertTemplateFiles($page);

        return $page;
    }

    private function RequestPage() {
        // https://stackoverflow.com/questions/28541876/how-to-get-full-html-of-web-page-via-php
        // https://www.php.net/manual/en/book.curl.php
        $ch = curl_init(); // Initialize a cURL session
        curl_setopt($ch, CURLOPT_URL, $this->pageUrl); // The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // true to return the transfer as a string of the return value of curl_exec() instead of outputting it directly.
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // true to follow any "Location: " header that the server sends as part of the HTTP header
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36"); // The contents of the "User-Agent: " header to be used in a HTTP request.
        $data = curl_exec($ch); // Perform a cURL session
        curl_close($ch); // Close a cURL session

        return $data;
    }

    private function UpdateFiles($page) {
        // EXTRACT THE URL FOR THE INITIALIZE JS FILE FOR LATER USE
        // WE DO IT DYNAMICALLY LIKE THIS BECAUSE THE SCRIPT URL CHANGES IN WCM RELEASES - WE WON'T HAVE TO KEEP IT UPDATED HERE
        $FindInitializeJsScript = preg_match('/<script.*?initialize.*?<\/script>/i', $page, $initializeScriptTag, PREG_OFFSET_CAPTURE);
        
        // WE'LL DO A LITTLE CHECK HERE TO MAKE SURE WE FOUND THE INITIALIZE FILE
        // IF WE DIDN'T, THAT MEANS THAT THERE'S SOMETHING WRONG WITH THE PROVIDED URL AND THE REQUESTED WCM PAGE DOESN'T EXIST
        // THE WCM PAGE REDIRECTS TO A 404 AND ACTUALLY NAVIGATES YOU AWAY FROM YOUR LOCAL HOST SITE TO THE WCM SITE YOU'RE REQUESTING
        if(!$FindInitializeJsScript) {
            echo $this->errorPage->PrintError("Some of the elements we're looking for in the requested WCM page weren't found so that means that there's something wrong with the url that you're trying to load. Double check your url values in the /src/classes/variables.php file and make sure they in fact point to a WCM site and that each page url is correct. The variables file was generated with the values you provided on the launch page.");
            
            exit;
        }
        $initializeScript = preg_match('/<script.*?src=("|\')(.*?)("|\')/i', $initializeScriptTag[0][0], $initializeJsFileSrc, PREG_OFFSET_CAPTURE);
        $initializeJsFileSrc = $initializeJsFileSrc[2][0];

        // EXTRACT THE URLS FOR THE CHECKSCRIPT JS FILE FOR LATER USE
        // WE DO IT DYNAMICALLY LIKE THIS BECAUSE THE SCRIPT URL CHANGES IN WCM RELEASES - WE WON'T HAVE TO KEEP IT UPDATED HERE
        $FindCheckScriptJsScript = preg_match('/<script.*?checkscript.*?<\/script>/i', $page, $checkScriptScriptTag, PREG_OFFSET_CAPTURE);
        $checkScriptScript = preg_match('/<script.*?src=("|\')(.*?)("|\')/i', $checkScriptScriptTag[0][0], $checkScriptJsFileSrc, PREG_OFFSET_CAPTURE);
        $checkScriptJsFileSrc = $checkScriptJsFileSrc[2][0];

        // GET FILE CONTENTS OF THE INITIALIZE FILE
        $initializeJsFile = file_get_contents($initializeJsFileSrc);
        // REMOVE THE AJAX CODE AND REPLACE WITH CODE TO SIMPLY CREATE AND ADD A SCRIPT ELEMENT TO THE HEAD
        $initializeJsFile = preg_replace('/\$\.each\(arrScripts\, function \(\) \{[\s\S]*?\}\)/i', "$.each(arrScripts, function () {script.src = this;$('head').append(script);", $initializeJsFile);

        // GET FILE CONTENTS OF THE CHECKSCRIPT FILE
        $checkScriptJsFile = file_get_contents($checkScriptJsFileSrc);
        // REMOVE THE AJAX CODE AND REPLACE WITH CODE TO SIMPLY CREATE AND ADD A SCRIPT ELEMENT TO THE HEAD
        // THE ONE ISSUE HERE IS THAT THE AJAX REQUEST IN THIS FILE HAS 'async' SET TO false - ADDING SCRIPTS THIS OTHER WAY IS ASYNCHRONOUS WHICH COULD BE PROBLEMATIC - SEE NEXT SECTION
        $checkScriptJsFile = preg_replace('/\$\.ajax\(\{[\s\S]*?\}\)/i', "var script = document.createElement('script');script.type = 'text/javascript';script.src = ScriptSRC;$('head').append(script);", $checkScriptJsFile);

        // ADD THE UPDATED INITIALIZE AND CHECKSCRIPT CODE BACK TO THE PAGE
        $page = str_replace("<!-- End swuc.CheckScript -->", '<script>' . $initializeJsFile . "\n" . $checkScriptJsFile . '</script><!-- End swuc.CheckScript -->', $page);

        return $page;
    }

    private function RunSystemReplacements($page) {
        // EXECUTE ALL OF THE NECESSARY REPLACEMENTS
        // STORED IN /includes/replacements.php

        // GLOBAL
        foreach($this->replacements->Get("globalSystemReplacements") as $find => $replace) {
            $page = preg_replace($find, $replace, $page);
        }

        // SUBPAGE ONLY
        if($this->pageID == "sp") {
            foreach($this->replacements->Get("subpageSystemReplacements") as $find => $replace) {
                $page = preg_replace($find, $replace, $page);
            }
        }

        return $page;
    }

    private function InsertTemplateFiles($page) {
        // TURN THE PAGE INTO AN HTML DOCUMENT FOR EASY TARGETING OF ELEMENTS
        $htmlPage = $this->DomifyPage($page);
        $templateFilePath = "";
        $pageSpecificReplacements = "";

        // BUILD THE THE REPLACEMENT ARRAY FOR THE TEMPLATE FILE
        switch($this->pageID) {
            case "sp":
                $templateFilePath = "./src/template/html/sp.html";
                $this->replacements->BuildSubpageTemplateReplacementArray($htmlPage);
                $pageSpecificReplacements = $this->replacements->Get("subpageTemplateReplacements");
            break;

            case "spn":
                $templateFilePath = "./src/template/html/spn.html";
                $this->replacements->BuildSubpageTemplateReplacementArray($htmlPage);
                $pageSpecificReplacements = $this->replacements->Get("subpageTemplateReplacements");
            break;
            case "mv":
                $templateFilePath = "./src/template/html/mv.html";
                $this->replacements->BuildSubpageTemplateReplacementArray($htmlPage);
                $pageSpecificReplacements = $this->replacements->Get("subpageTemplateReplacements");
            break;
            case "hp":
            default:
                $templateFilePath = "./src/template/html/hp.html";
                $this->replacements->BuildHomepageTemplateReplacementArray($htmlPage);
                $pageSpecificReplacements = $this->replacements->Get("homepageTemplateReplacements");
            break;
        }

        // REQUEST THE TEMPLATE FILES
        $templateFile = file_get_contents($templateFilePath);
        $headContent = file_get_contents("./src/template/compiled/html/head.html");
        
        if(!file_exists("./src/template/compiled/css/1024.css")) {
            echo $this->errorPage->PrintError('You must run "npm run compile" in the terminal before you can view your webpage. Your skeleton files need to be initially compiled after launch.');

            exit;
        }

        $cssFilePaths = array(
            "desktop" => "./src/template/compiled/css/1024.css",
            "768" => "./src/template/compiled/css/768.css",
            "640" => "./src/template/compiled/css/640.css",
            "480" => "./src/template/compiled/css/480.css",
            "320" => "./src/template/compiled/css/320.css"
        );

        $cssFiles = array(
            "desktop" => file_get_contents($cssFilePaths["desktop"]),
            "768" => file_get_contents($cssFilePaths["768"]),
            "640" => file_get_contents($cssFilePaths["640"]),
            "480" => file_get_contents($cssFilePaths["480"]),
            "320" => file_get_contents($cssFilePaths["320"])
        );

        // PREP THE ARRAY OF GLOBAL TEMPLATE FILE REPLACEMENTS
        $this->replacements->BuildGlobalTemplateReplacementArray($htmlPage);
        $globalTemplateReplacements = $this->replacements->Get("globalTemplateReplacements");
     
        // EXECUTE ALL OF THE NECESSARY GLOBAL TEMPLATE FILE REPLACEMENTS - STORED IN /includes/replacements.php
        $templateFile = strtr($templateFile, $globalTemplateReplacements);
        $templateFile = $this->replacements->replaceComplexGlobalActiveBlocks($templateFile);
        $headContent = strtr($headContent, $globalTemplateReplacements);
        $headContent = $this->replacements->replaceComplexGlobalActiveBlocks($headContent);
        $cssFiles["desktop"] = strtr($cssFiles["desktop"], $globalTemplateReplacements);
        $cssFiles["768"] = strtr($cssFiles["768"], $globalTemplateReplacements);
        $cssFiles["640"] = strtr($cssFiles["640"], $globalTemplateReplacements);
        $cssFiles["480"] = strtr($cssFiles["480"], $globalTemplateReplacements);
        $cssFiles["320"] = strtr($cssFiles["320"], $globalTemplateReplacements);

        // EXECUTE ALL OF THE NECESSARY TEMPLATE FILE SPECIFIC REPLACEMENTS - STORED IN /includes/replacements.php
        $templateFile = strtr($templateFile, $pageSpecificReplacements);

        // WRITE THE UPDATED CSS BACK TO THE FILES
        foreach($cssFiles as $breakpoint => $file) {
            $cssFile = fopen($cssFilePaths[$breakpoint], "w") or die("Unable to open " . $breakpoint . " CSS file!");
            fwrite($cssFile, $file);
            fclose($cssFile);
        }

        // CREATE CACHE FILE
        if(!is_dir("./src/cache/")) {
            mkdir("./src/cache/", 0777);
        }
        if(!file_exists("./src/cache/$this->pageID.php")) {
            $cacheFile = fopen("./src/cache/$this->pageID.php", "w") or die("Unable to create $this->pageID.php cache file!");
            fwrite($cacheFile, $page);
            fclose($cacheFile);
        }

        // INSERT THE CSS AND HEAD CONTENT INTO THE PAGE
        $page = preg_replace('/<!-- Page -->[\s\S]*?<!-- App Preview -->/', '<!-- Page --><link rel="stylesheet" type="text/css" href="./src/template/css/index.css" />' . $headContent . '<!-- App Preview -->', $page);
        
        // INSERT THE TEMPLATE FILE INTO THE PAGE
        $page = preg_replace('/<!-- END - MYSTART BAR -->[\s\S]*?<!-- BEGIN - STANDARD FOOTER -->/', '<!-- END - MYSTART BAR -->' . $templateFile . '<!-- BEGIN - STANDARD FOOTER -->', $page);
        
        return $page;
    }

    private function DomifyPage($page) {
        // TURN OFF WARNING/ERROR LOGGING 
        // https://stackoverflow.com/questions/14783760/remove-dom-warning-php
        // DOMDocument is very good at dealing with imperfect markup, but it throws warnings all over the place when it does.
        libxml_use_internal_errors(true);

        $htmlPage = new DomDocument;
        if(!$htmlPage->loadHTML($page, LIBXML_SCHEMA_CREATE)) { // LIBXML_SCHEMA_CREATE - https://stackoverflow.com/questions/4029341/dom-parser-that-allows-html5-style-in-script-tag
            libxml_clear_errors();
        }

        return $htmlPage;
    }

}

?>