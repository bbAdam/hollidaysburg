<?php

// SETTING UP THIS WAY HELPS KEEP THINGS OUT OF THE GLOBAL SPACE WHILE HAVING GLOBAL ACCESS
class Replacements {

    function __construct() {
        $variables = new Variables();

        // SET PROPERTIES
        $this->loggedIn = (isset($_GET["loggedin"]) && $_GET["loggedin"] == "true") ? true : false;
        $this->pageID = (isset($_GET["page"])) ? $_GET["page"] : "hp";
        $this->siteDomain = $variables->Get("siteDomain");
        $this->replacements = array();

        // BUILD EACH ARRAY
        $this->BuildGlobalSystemReplacementArray($variables);
        $this->BuildSubpageSystemReplacementArray($variables);
    }

    public function Get($replacementSet) {
        if(array_key_exists($replacementSet, $this->replacements)) {
            return $this->replacements[$replacementSet];
        } else {
            return null;
        }
    }

    private function BuildGlobalSystemReplacementArray($variables) {
        $this->replacements["globalSystemReplacements"] = array(
            // FIND RELATIVE SRC PATHS AND PREPEND THE DOMAIN TO MAKE ABSOLUTE
            '/src=\"\.\.\//i' => 'src="'  . $variables->Get("siteDomain") . '/',
            '/src=\'\.\.\//i' => "src='"  . $variables->Get("siteDomain") . "/",
            '/src=\"\/{1}(?!\/)/i' => 'src="'  . $variables->Get("siteDomain") . '/',
            '/src=\'\/{1}(?!\/)/i' => "src='"  . $variables->Get("siteDomain") . "/",
        
            // FIND RELATIVE HREF PATHS AND PREPEND THE DOMAIN TO MAKE ABSOLUTE
            '/href=\"\.\.\//i' => 'href="'  . $variables->Get("siteDomain") . '/',
            '/href=\'\.\.\//i' => "href='"  . $variables->Get("siteDomain") . "/",
            '/href=\"\/{1}(?!\/)/i' => 'href="'  . $variables->Get("siteDomain") . '/',
            '/href=\'\/{1}(?!\/)/i' => "href='"  . $variables->Get("siteDomain") . "/",
        
            // FIND RELATIVE CMS PATHS AND PREPEND THE DOMAIN TO MAKE ABSOLUTE
            '/\"\/cms\/lib/i' => '"' . $variables->Get("siteDomain") . '/cms/lib',
            '/\'\/cms\/lib/i' => "'" . $variables->Get("siteDomain") . '/cms/lib',
        
            // REMOVE THE SYSTEM INITIALIZE AND CHECKSCRIPT SCRIPTS BECAUSE THEY CONTAIN AJAX REQUESTS AND THROW CORS ERRORS
            // WE ALREADY GOT THE CODE FROM THESE FILES, UPDATED THE CALLS AND ADDED THE CODE BACK TO THE PAGE
            '/<script.*?initialize.*?<\/script>/i' => "",
            '/<script.*?checkscript.*?<\/script>/i' => "",

            // REMOVE THE DASHBOARD CSS FILE - IT'S NOT NEEDED AND THROWS A CORS ERROR ON THE GOOGLE FONT FILE
            '/<link.*?dashboard.*?\/>/i' => "",
        );

        if($this->loggedIn) {
            $this->replacements["globalSystemReplacements"]['/<div.*?ui-btn-signin.*?<\/div>/i'] = "<div class='sw-mystart-nav sw-mystart-button manage'><a id='ui-btn-sitemanager' onclick='' tabindex='0'><span>Site Manager</span></a></div><div id='sw-mystart-account'><div id='sw-myaccount' class='sw-mystart-nav' tabindex='0' title='Edit your account settings or sign out.'>My Account<span class='sw-myaccount-chevron' aria-hidden='true'>&nbsp;</span></div><ul id='sw-myaccount-list' class='sw-dropdown'><li><a href='#' class='sw-accountsettings'><span>Edit Account Settings</span></a></li><li><a href='#' class='sw-signout'><span>Sign Out</span></a></li></ul></div><div class='sw-mystart-button' id='sw-mystart-mypasskey'><a id='ui-btn-mypasskey' href='javascript:;' aria-label='My Passkeys' aria-haspopup='true' role='button' aria-controls='ui-mypasskey-overlay' tabindex='0' class='ui-detail-overlay' actionClass='active' targetDivID='ui-mypasskey-overlay' position='bottomleft' targetDivJS=''><span>My PassKeys</span></a></div>";
            $this->replacements["globalSystemReplacements"]['/<div.*?ui-btn-register.*?<\/div>/i'] = "";
            $this->replacements["globalSystemReplacements"]['/<\/div>\s*<div.*?sw-mystart-right.*?>/i'] = "<div class='sw-mystart-nav sw-mystart-button pw'><a id='ui-btn-myview' tabindex='0' href='#'><span>MyView</span></a></div></div><div id='sw-mystart-right'>";
        }
    }

    private function BuildSubpageSystemReplacementArray($variables) {
        $this->replacements["subpageSystemReplacements"] = array(
            // CHECKSCRIPT DOESN'T GET THE FILE LOADED IN TIME AND A 'WCM is not defined' ERROR GETS THROWN FROM THE PAGELIST NAV
            // WE'LL JUST REMOVE THE CHECKSCRIPT CALL AND ADD THE FILE DIRECTLY
            '/<script type="text\/javascript">[\s]*?\$\(document\).ready\(function \(\) \{[\s]*?CheckScript\(\'\', staticURL \+ \'GlobalAssets\/Scripts\/sw-ada.js\'\);[\s\S]*?<\/script>/i' => '<script type="text/javascript" src="' . $variables->Get("siteDomain") . '/Static/GlobalAssets/Scripts/sw-ada.js"></script><script type="text/javascript">$(document).ready(function () {WCM.ADA.tree = new WCM.ADA.Tree();});</script>',
        );
    }

    public function BuildGlobalTemplateReplacementArray($htmlPage) {
        $variables = new Variables();
        $this->replacements["globalTemplateReplacements"] = array();

        // CHANNEL BAR
        $channelBar = $htmlPage->getElementById("sw-channel-list-container");
        $channelBarCheck = $channelBar;
        $channelBar = $htmlPage->saveHTML($channelBar);
        if($channelBarCheck != null) { 
            $channelBarScript = $htmlPage->getElementById("sw-channel-list-container")->nextSibling->nextSibling; // TWO nextSibling CALLS BECAUSE OF WHITE SPACE BETWEEN CLOSING CHANNEL BAR DIV AND SCRIPT TAG
            $channelBarScript = $htmlPage->saveHTML($channelBarScript);
            $this->replacements["globalTemplateReplacements"]['[$ChannelListNavigation props="DisplayType:Text;SectionMax:<SWCtrl controlname="Custom" props="Name:sectionMax" />;DirectoryType:6;DisplayHomeButton:<SWCtrl controlname="Custom" props="Name:showHomeChannel" />;DisplayCalendarButton:<SWCtrl controlname="Custom" props="Name:showCalendarChannel" />;HideSingleSectionDD:<SWCtrl controlname="Custom" props="Name:hideSingleSectionDD" />"$]'] = $channelBar . $channelBarScript;
        }

        // GENERAL ACTIVE BLOCKS
        $this->replacements["globalTemplateReplacements"]['[$SiteName$]'] = $variables->Get("siteName");
        $this->replacements["globalTemplateReplacements"]['[$SiteAlias$]'] = $variables->Get("siteAlias");
        $this->replacements["globalTemplateReplacements"]['[$SiteAddressOne$]'] = $variables->Get("siteAddress");
        $this->replacements["globalTemplateReplacements"]['[$SiteCity$]'] = $variables->Get("siteCity");
        $this->replacements["globalTemplateReplacements"]['[$SiteState$]'] = $variables->Get("siteState");
        $this->replacements["globalTemplateReplacements"]['[$SiteZip$]'] = $variables->Get("siteZip");
        $this->replacements["globalTemplateReplacements"]['[$SiteContactPhone$]'] = $variables->Get("sitePhone");
        $this->replacements["globalTemplateReplacements"]['[$SiteContactFax$]'] = $variables->Get("siteFax");
        $this->replacements["globalTemplateReplacements"]['[$SiteMapLink$]'] = '<a href="#" target="_self"><span>Site Map</span></a>';
        $this->replacements["globalTemplateReplacements"]['<swctrl controlname="SkipNav" props=""/>'] = '<a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a>';
        $this->replacements["globalTemplateReplacements"]['<swctrl controlname="SkipNav" props="" />'] = '<a id="sw-maincontent" name="sw-maincontent" tabindex="-1"></a>';
        $this->replacements["globalTemplateReplacements"]['[$Search$]'] = '<label for="swsitesearch-txt-searchstring" class="hidden-label">Search Our Site</label><input id="swsitesearch-txt-searchstring" type="text" title="Search Term" aria-label="Search Our Site" placeholder="Search this Site..." /><input id="" aria-label="Submit Site Search" type="button" value="Search" onclick="" />';
        
        if(isset($_GET['channelName'])) {
            $this->replacements["globalTemplateReplacements"]['[$ChannelName$]'] = $_GET['channelName'];
        } else {
            $this->replacements["globalTemplateReplacements"]['[$ChannelName$]'] = "No Channel Name Set";
        }
        if(isset($_GET['sectionName'])) {
            $this->replacements["globalTemplateReplacements"]['[$SectionName$]'] = $_GET['sectionName'];
        } else {
            $this->replacements["globalTemplateReplacements"]['[$SectionName$]'] = "No Section Name Set";
        }
        if(isset($_GET['parentName'])) {
            $this->replacements["globalTemplateReplacements"]['[$PN$]'] = $_GET['parentName'];
        } else {
            $this->replacements["globalTemplateReplacements"]['[$PN$]'] = "No Parent Name Set";
        }
        if(isset($_GET['channelID'])) {
            $this->replacements["globalTemplateReplacements"]['[$ChannelID$]'] = $_GET['channelID'];
        } else {
            $this->replacements["globalTemplateReplacements"]['[$ChannelID$]'] = "0";
        }
        if(isset($_GET['sectionID'])) {
            $this->replacements["globalTemplateReplacements"]['[$SectionID$]'] = $_GET['sectionID'];
        } else {
            $this->replacements["globalTemplateReplacements"]['[$SectionID$]'] = "0";
        }
        if(isset($_GET['siteID'])) {
            $this->replacements["globalTemplateReplacements"]['[$SiteID$]'] = $_GET['siteID'];
        } else {
            $this->replacements["globalTemplateReplacements"]['[$SiteID$]'] = "0";
        }

        // ELEMENTS
        // GET THE ELEMENTS FILE AND CONVERT THE JSON TO AN ASSOCIATIVE ARRAY
        $elementsFile = file_get_contents("./src/template/elements/elements.json");
        $elements = json_decode($elementsFile, true);
        
        // LOOP THROUGH EACH ELEMENT SET 
        foreach($elements as $elementSetName => $elementSet) {
            // LOOP THROUGH EACH ELEMENT IN THE SET
            if(is_array($elementSet)) {
                foreach($elementSet as $element) {
                    $elementValue = ($element["type"] == "dropdown") ? (explode(";", $element["default"]))[0] : $element["default"];
                    $this->replacements["globalTemplateReplacements"]['<SWCtrl controlname="Custom" props="Name:' . $element["name"] . '" />'] = $elementValue;
                }
            }
        }
    }

    public function replaceComplexGlobalActiveBlocks($templateFile) {
        // REPLACE IF LOGGED IN ACTIVE BLOCK
        // LOGGED IN
        if($this->loggedIn) {
            $templateFile = preg_replace('/\[\$if logged in\$\]|\[\$else if logged\$\][\s\S]*?\[\$end if logged in\$\]/i', '', $templateFile);
        }
        
        // NOT LOGGED IN
        else {
            $templateFile = preg_replace('/\[\$if logged in\$\][\s\S]*?\[\$else if logged\$\]|\[\$end if logged in\$\]/i', '', $templateFile);
        }

        return $templateFile;
    }

    public function BuildHomepageTemplateReplacementArray($htmlPage) {
        $this->replacements["homepageTemplateReplacements"] = array();

        // CONTENT REGION 1
        $region1 = $htmlPage->getElementById("sw-content-container1");
        $region1Value = ($region1 != null) ? $htmlPage->saveHTML($region1) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:1"$]'] = $region1Value;

        // CONTENT REGION 2
        $region2 = $htmlPage->getElementById("sw-content-container2");
        $region2Value = ($region2 != null) ? $htmlPage->saveHTML($region2) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:2"$]'] = $region2Value;

        // CONTENT REGION 3
        $region3 = $htmlPage->getElementById("sw-content-container3");
        $region3Value = ($region3 != null) ? $htmlPage->saveHTML($region3) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:3"$]'] = $region3Value;

        // CONTENT REGION 4
        $region4 = $htmlPage->getElementById("sw-content-container4");
        $region4Value = ($region4 != null) ? $htmlPage->saveHTML($region4) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:4"$]'] = $region4Value;

        // CONTENT REGION 5
        $region5 = $htmlPage->getElementById("sw-content-container5");
        $region5Value = ($region5 != null) ? $htmlPage->saveHTML($region5) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:5"$]'] = $region5Value;

        // CONTENT REGION 6
        $region6 = $htmlPage->getElementById("sw-content-container6");
        $region6Value = ($region6 != null) ? $htmlPage->saveHTML($region6) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:6"$]'] = $region6Value;

        // CONTENT REGION 7
        $region7 = $htmlPage->getElementById("sw-content-container7");
        $region7Value = ($region7 != null) ? $htmlPage->saveHTML($region7) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:7"$]'] = $region7Value;

        // CONTENT REGION 8
        $region8 = $htmlPage->getElementById("sw-content-container8");
        $region8Value = ($region8 != null) ? $htmlPage->saveHTML($region8) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:8"$]'] = $region8Value;

        // CONTENT REGION 9
        $region9 = $htmlPage->getElementById("sw-content-container9");
        $region9Value = ($region9 != null) ? $htmlPage->saveHTML($region9) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:9"$]'] = $region9Value;

        // CONTENT REGION 10
        $region10 = $htmlPage->getElementById("sw-content-container10");
        $region10Value = ($region10 != null) ? $htmlPage->saveHTML($region10) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:10"$]'] = $region10Value;

        // CONTENT REGION 11
        $region11 = $htmlPage->getElementById("sw-content-container11");
        $region11Value = ($region11 != null) ? $htmlPage->saveHTML($region11) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:11"$]'] = $region11Value;

        // CONTENT REGION 12
        $region12 = $htmlPage->getElementById("sw-content-container12");
        $region12Value = ($region12 != null) ? $htmlPage->saveHTML($region12) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:12"$]'] = $region12Value;

        // CONTENT REGION 13
        $region13 = $htmlPage->getElementById("sw-content-container13");
        $region13Value = ($region13 != null) ? $htmlPage->saveHTML($region13) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:13"$]'] = $region13Value;

        // CONTENT REGION 14
        $region14 = $htmlPage->getElementById("sw-content-container14");
        $region14Value = ($region14 != null) ? $htmlPage->saveHTML($region14) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:14"$]'] = $region14Value;

        // CONTENT REGION 15
        $region15 = $htmlPage->getElementById("sw-content-container15");
        $region15Value = ($region15 != null) ? $htmlPage->saveHTML($region15) : "";
        $this->replacements["homepageTemplateReplacements"]['[$ContentRegion props="ContainerNumber:15"$]'] = $region15Value;
    }

    public function BuildSubpageTemplateReplacementArray($htmlPage) {
        $variables = new Variables();
        $this->replacements["subpageTemplateReplacements"] = array();

        // SINCE WE HAVE TO TARGET SOME ELEMENTS VIA CLASS AND DOMDOCUMENT DOES NOT HAVE A getElementsByClassName METHOD, WE HAVE TO USE DOMMXpath
        // https://stackoverflow.com/questions/33441697/call-to-undefined-method-domdocumentgetelementsbyclassname/33446305#33446305
        // https://www.php.net/manual/en/class.domxpath.php
        $xpath = new DOMXpath($htmlPage);

        // PAGE LIST NAVIGATION
        $pageListNavFound = false;
        foreach($xpath->evaluate('//div[contains(@class, "pagenavigation")]') as $pageListNav) {
            $this->replacements["subpageTemplateReplacements"]['[$PageListNavigation props="DisplayHeading:Yes"$]'] = $htmlPage->saveHTML($pageListNav);
            $this->replacements["subpageTemplateReplacements"]['[$PageListNavigation props="DisplayHeading:No"$]'] = $htmlPage->saveHTML($pageListNav);
        
            $pageListNavFound = true;
        }
        if(!$pageListNavFound) {
            $this->replacements["subpageTemplateReplacements"]['[$PageListNavigation props="DisplayHeading:Yes"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$PageListNavigation props="DisplayHeading:No"$]'] = "";
        }

        // SECTION LIST NAVIGATION
        $sectionListNavFound = false;
        foreach($xpath->evaluate('//div[contains(@class, "sectionnavigation")]') as $sectionListNav) {
            $this->replacements["subpageTemplateReplacements"]['[$SectionListNavigation props="DisplayHeading:Yes"$]'] = $htmlPage->saveHTML($sectionListNav);
            $this->replacements["subpageTemplateReplacements"]['[$SectionListNavigation props="DisplayHeading:No"$]'] = $htmlPage->saveHTML($sectionListNav);
        
            $sectionListNavFound = true;
        }
        if(!$sectionListNavFound) {
            $this->replacements["subpageTemplateReplacements"]['[$SectionListNavigation props="DisplayHeading:Yes"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$SectionListNavigation props="DisplayHeading:No"$]'] = "";
        }

        // BREADCRUMBS
        $breadCrumbsFound = false;
        foreach($xpath->evaluate('//ul[contains(@class, "ui-breadcrumbs")]') as $breadCrumbs) {
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:1"$]'] = $htmlPage->saveHTML($breadCrumbs);
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:2"$]'] = $htmlPage->saveHTML($breadCrumbs);
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:3"$]'] = $htmlPage->saveHTML($breadCrumbs);
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:4"$]'] = $htmlPage->saveHTML($breadCrumbs);
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:5"$]'] = $htmlPage->saveHTML($breadCrumbs);
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:6"$]'] = $htmlPage->saveHTML($breadCrumbs);

            $breadCrumbsFound = true;
        }
        if(!$breadCrumbsFound) {
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:1"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:2"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:3"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:4"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:5"$]'] = "";
            $this->replacements["subpageTemplateReplacements"]['[$Breadcrumb props="MaxLevels:6"$]'] = "";
        }

        // CONTENT LAYOUT PLACEHOLDER
        if($this->pageID == "mv") {
            $ContentLayoutPlaceholder = $htmlPage->getElementById("sw-content-layout-wrapper");
            if($ContentLayoutPlaceholder != null) { $this->replacements["subpageTemplateReplacements"]['[$ContentLayoutPlaceholder$]'] = '<div id="pw-main-wrapper" style="text-align: center;">MyView content container will render here.</div>'; }
        } else {
            $ContentLayoutPlaceholder = $htmlPage->getElementById("sw-content-layout-wrapper");
            if($ContentLayoutPlaceholder != null) { $this->replacements["subpageTemplateReplacements"]['[$ContentLayoutPlaceholder$]'] = $htmlPage->saveHTML($ContentLayoutPlaceholder); }
        }
    }

}

?>