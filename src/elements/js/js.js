$(function() {
    ElementManager.Init();
});

var ElementManager = {
	// PROPERTIES
    
    
    // METHODS
    "Init": function() {
        // FOR SCOPE
        var _this = this;
        
        this.LoadElements();
        this.ElementExample();

        $("#generate").on("click",function(){ _this.ElementGenerator(); });
    },

    "ElementExample": function(){
    	$("#element-syntax-toggle").click(function(){
    		if($(this).hasClass("open")){
    			closeHelper();
    		} else {
    			openHelper();
    		}
    	})

    	//OPEN SYNTAX HELPER
    	function openHelper(){
    		$("#element-syntax").addClass("open");
			$("#element-syntax-toggle").addClass("open").text("hide element syntax (ESC)");
    	}
    	//CLOSE SYNTAX HELPER
    	function closeHelper(){
    		$("#element-syntax").removeClass("open");
			$("#element-syntax-toggle").removeClass("open").text("view element syntax (ESC)");
    	}
    	//ESC SHORTCUT
    	$("#element-creator").on("keydown", function(e) {
    		switch(e.keyCode){
    			case 27:
    				if($("#element-syntax-toggle").hasClass("open")){
		    			closeHelper();
		    		} else {
		    			openHelper();
		    		}
    			break;
    		}
    	})
    },

    "LoadElements": function(){

    	var savedElements = "";
    	var elementType ="";
    	var elementSetCount = 0;

    	$.getJSON( "../template/elements/elements.json", function( data ) {
			  $.each( data, function( key, elements ) {

			  	if(elementSetCount == 0){
			  		savedElements += "//"+key+"\n";
			  	} else {
			  		savedElements += "\n//"+key+"\n";
			  	}
			  	
			    $.each( elements, function( index, element ) {
			    	savedElements += element.name+":";

			    	switch(element.type){
			    		case "color":
							elementType = "1";
						break;
						case "text":
							elementType = "2";
						break;
						case "boolean":
							elementType = "3";
						break;
						case "number":
							elementType = "4";
						break;
						case "radio":
							elementType = "5";
						break;
						case "dropdown":
							elementType = "6";
						break;
						case "icon":
							elementType = "7";
						break;
						case "image":
							elementType = "8";
						break;
						default:
							elementType = "2";
			    	}

			    	savedElements += elementType+":"+element.default+":"+element.label;

			    	if(!element.description == ""){
			    		savedElements += ":"+element.description+"\n";
			    	} else {
			    		savedElements += "\n";
			    	}
			    })

			    elementSetCount++;

			  });
			 
			  $('#element-creator').val($.trim(savedElements));

			  //GENERATE ELEMENTS FOR DEV ON PAGE LOAD
			  $("#generate").click();
		});
    },

    "ElementGenerator": function(){

    	var _this = this;

    	//GRAB TEXT FROM TEXT AREA AND PLACE IN A VARIABLE AND CLEAN UP ANY EXTRA LINE BREAKS
		var cleanTextAreaLines = $.trim($('#element-creator').val());
		var replaceExtraLines = cleanTextAreaLines.replace(/[\r\n]\s*\n/g, '\n\n');
		var textAreaLines = replaceExtraLines.split('\n');

		//INITIALIZE OUR ELEMENTS SETS VARIABLES
		var elementSets = "{\n";
		var elementsForDev = "";
		//NEED A VARIABLE TO COUNT ELEMENTS WITHIN A SET SO WE KNOW WHEN WE ARE ON THE FIRST ELEMENT IN EACH SET (THESE ONES WILL NOT RECEIVE A PRECEDING ,)
		var elementCounter = 0;

		//LOOP THROUGH EACH LINE IN THE TEXT AREA
		$.each(textAreaLines, function(){
			//PLACE THE CONTENTS OF THE CURRENT LINE INTO A VARIABLE
			var rawElement = this.trim();

			//CHECK TO SEE IF WE ARE AT THE START OF A SET,
			//WITHIN A SET, OR AT THE END OF A SET
			if(rawElement.indexOf("//") >= 0){ //THIS IS THE START OF AN ELEMENT SET
				
				//GET A CLEAN ELEMENT SET NAME
				var rawElementSetName = this.split("/");
				var elementSetName = rawElementSetName[2];

				//FOR DEV
				elementsForDev += this+'\n';

				//CREATE THE OPENING CODE FOR AN ELEMENT SET
				elementSets += 	'\t"'+elementSetName+'": [';

				//UPDATE THE ELEMENT COUNTER, THE NEXT LINE WE ENCOUNTER WILL BE THE FIRST ELEMENT IN THIS SET
				elementCounter++;
			} else if(rawElement == ""){ //THIS IS THE END OF AN ELEMENT SET
				
				//CREATE THE CODE FOR CLOSING AN ELEMENT SET
				elementSets += 		'\n\t],\n';

				//FOR DEV
				elementsForDev += '\n';

				//RESET THE ELEMENT COUNTER, THIS WAS THE LAST ELEMENT IN THIS SET
				elementCounter = 0;
			} else { //THIS IS AN ELEMENT WITHIN THE SET

				var newElement = this.split(":");

				//CHECK ELEMENT TYPE
				var elementType = "";
				switch(newElement[1]){
					case "1":
						elementType = "color";
					break;
					case "2":
						elementType = "text";
					break;
					case "3":
						elementType = "boolean";
					break;
					case "4":
						elementType = "number";
					break;
					case "5":
						elementType = "radio";
					break;
					case "6":
						elementType = "dropdown";
					break;
					case "7":
						elementType = "icon";
					break;
					case "8":
						elementType = "image";
					break;
					default:
						elementType = "text";
				}

				if(elementCounter == 1){
					if(newElement.length == 4){ // THIS ELEMENT DOES NOT HAVE A DESCRIPTION
						elementSets += '\n\t\t{\n\t\t\t"name":"'+newElement[0]+'",\n\t\t\t"type":"'+elementType+'",\n\t\t\t"default":"'+newElement[2]+'",\n\t\t\t"label":"'+newElement[3]+'",\n\t\t\t"description":""\n\t\t}';
					} else { // THIS ELEMENT HAS A DESCRIPTION
						elementSets += '\n\t\t{\n\t\t\t"name":"'+newElement[0]+'",\n\t\t\t"type":"'+elementType+'",\n\t\t\t"default":"'+newElement[2]+'",\n\t\t\t"label":"'+newElement[3]+'",\n\t\t\t"description":"'+newElement[4]+'"\n\t\t}';
					}
				} else {
					if(newElement.length == 4){ // THIS ELEMENT DOES NOT HAVE A DESCRIPTION
						elementSets += ',\n\t\t{\n\t\t\t"name":"'+newElement[0]+'",\n\t\t\t"type":"'+elementType+'",\n\t\t\t"default":"'+newElement[2]+'",\n\t\t\t"label":"'+newElement[3]+'",\n\t\t\t"description":""\n\t\t}';
					} else { // THIS ELEMENT HAS A DESCRIPTION
						elementSets += ',\n\t\t{\n\t\t\t"name":"'+newElement[0]+'",\n\t\t\t"type":"'+elementType+'",\n\t\t\t"default":"'+newElement[2]+'",\n\t\t\t"label":"'+newElement[3]+'",\n\t\t\t"description":"'+newElement[4]+'"\n\t\t}';
					}
				}

				//FOR DEV
				elementsForDev += '<SWCtrl controlname="Custom" props="Name:'+newElement[0]+'" />\n';
				
				//UPDATE THE ELEMENT COUNTER
				elementCounter++;
			}

		});

		//CLOSE THE STRING
		elementSets += "\n\t]\n}";

		//UPDATE ELEMENTS FOR DEV 
		$('#elements-for-dev').val($.trim(elementsForDev));

		//WRITE TO JSON FILE
		generateElementFile({
			"callback" : function(props){
				$.ajax({
			        url: 'elements.php',
			        type: 'POST',
			        dataType: "json",
			        data: {
			        	elementJson: props.elementJson
			        },
			        success: function(data){
			        	//alert('elements have been generated');
			        }
			    });
			}
		});

		function generateElementFile(props){
			//GET VARIABLES FROM PAGE
			var elementJson = elementSets;
				
			//PLACE ALL OF OUR NEW CODE INTO THE PAGE
			if(props !== undefined ) {
				if(props.callback !== undefined){
					props.callback({
						'elementJson': elementJson
					});
				}
			}
		}
    }
}
