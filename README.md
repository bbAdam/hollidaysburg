CREATIVE SERVICES LOCAL IDE

This IDE was developed to take advantage of development tools and techniques that are not available when coding in WCM's Styler.


--------------------------------
SET UP ENVIRONMENT
--------------------------------

1. Fork the dev environment on GitLab and set up your new project repo.
2. Clone down your new project repo on your computer.
3. Launch MAMP.
	3.a. In preferences under "Ports", set "8888" for "Apache Port".
	3.b. In preferences under "Web Server" or "Server", point the "Document Root" folder to the root of your project directory or to your designated environment directory.
	3.c. Click "Start Servers".
4. Be sure you're still in your project repo in terminal. Run "npm install" to install all dependecies.
5. IMPORTANT: If you pointed your "Document Root" folder to the root of your project directory, you can skip this step. If you did not and you pointed it to some other directory where you will access your project webpage via something like "http://localhost:8888/some-project/", you will need to update the "localHostPath" field in the package.json file. So for example, if you are accessing "http://localhost:8888/some-project/" for your project webpage, update the "localHostPath" to "/some-project/". There are some scripts in the environment that must use absolute paths and we need to make sure to provide the correct path to the "/src" folder relative to where you are assigning your "Document Root" folder in MAMP.
6. Run "npm run launch". This will ask you a series of questions to set up the global variables for the dev environment, set your template files based on the chosen template skeleton and then compile the template files so that you're ready to launch the site and begin development. Your template files for development will be in /src/template.
7. Open your browser and navigate to http://localhost:8888/.
8. Run "npm run watch" to make webpack watch for your file changes. Now whenever you make a file change and save, webpack will automatically compile your template files and prepare them for your page refresh. Run "ctrl+c" in the terminal to stop the watch.


--------------------------------
AUTOMATIC COMPILE ON FILE SAVE
--------------------------------

1. Run "npm run watch" in the terminal. This will fire the nodemon module and will watch the following files. Any time any of the files are changed and saved, the "npm run compile" command will fire and compile all necessary template files for a page refresh in your browser. NOTE: Only the files below will be watched and/or compiled. If you need to add more files, you will need to manually update the webpack.config.js and package.json files.
	- ./src/template/css/1024.scss
	- ./src/template/css/768.scss
	- ./src/template/css/640.scss
	- ./src/template/css/480.scss
	- ./src/template/css/320.scss
	- ./src/template/js/head.js
2. Run "ctrl+c" in the terminal to stop the watch.


--------------------------------
BUILD TEMPLATE ZIP FILE
--------------------------------

1. Run "npm run build" in the terminal. This process will compile and prepare your files for the zip process.
2. You will be asked a series of questions:
	2.a. Template Name - Give your template a name
	2.b. Template Description - Give your template a description
		2.b.i.   Press "Enter" to open editor.
		2.b.ii.  Press "i" to begin inserting text. Type your multiline description.
		2.b.iii. When your description is done, press "esc".
		2.b.iv.  Press ":wq" to write your text and quit the editor.
	2.c. Template Virtual Asset Path - The "lib/ACCOUNT_NUMBER" value from the asset urls on your sandbox site.
3. The process with finish building your zip file and will be placed in the /dist directory.


--------------------------------
ELEMENTS
--------------------------------

- Elements are created and managed here: http://localhost:8888/src/elements

--------------------------------
THINGS TO KEEP IN MIND
--------------------------------

- The development environment caches the WCM pages that are requested in order to greatly improve the performance of the environment. You must clear this cache when you change anything (like content) on your WCM site because those changes will not show until you do. You need to get a fresh pull of your page so run "npm run clear-cache" in the terminal and then refresh your page. You will see your updates and the page will be re-cached. You also need to clear the cache when you switch to the logged in state or vice-versa.


--------------------------------
WORKSPACES
--------------------------------
The Creative Services Local IDE is a time-saver in many ways, the most powerful one, however, is its' utilization of Chrome Workspaces. The IDE uses Workspaces to track and sync files so that they can be edited in the Inspector and saved / viewed in real-time.

For more information on how to setup a workspace for your template project, go here: https://trello.com/c/CR38D9Oe/25-localhost-dev-how-to-video